#!/usr/bin/env bash
set -euo pipefail

if [[ ! -v QUIET ]]; then
	QUIET=true
fi

# The directory of this script is also LaTeM's root directory.
LATEM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

GRADLE_ARGS=(
	--console plain
)
if $QUIET; then
	GRADLE_ARGS+=(--quiet)
else
	GRADLE_ARGS+=(
		--info
		--stacktrace
	)
fi

export LATEM_TEMPLATE_SEARCH_PATH="${LATEM_ROOT}/templates"
export LATEM_WORKING_DIRECTORY="${PWD}"

cd "${LATEM_ROOT}"
# Quoting fix, using ${*@Q}, as per https://stackoverflow.com/a/10836225/194894
exec ${GRADLE:-./gradlew} ${GRADLE_ARGS[@]} \
	 latem-cli:run \
	 --args="${*@Q}"
