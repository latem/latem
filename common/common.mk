TEX_FIGURES_PATH := $(LATEX_PROJECT_ROOT)/figures
TEX_FIGURES := $(wildcard $(TEX_FIGURES_PATH)/*.tex)
SECTION_TEX_PATH := $(LATEX_PROJECT_ROOT)/sections
SECTION_TEX_FILES := $(wildcard $(SECTION_TEX_PATH)/*.tex)
GRAPHICS_PATH := $(LATEX_PROJECT_ROOT)/graphics
GRAPHICS_FILES := $(wildcard $(GRAPHICS_PATH)/*)

NPROC := $(shell nproc)
JOBS := $(shell echo $$(( $(NPROC) + 1)))
LOAD := $(shell echo "$(NPROC) * 1.35" | bc)
MAKE_PARALLEL_ARGS := -j$(JOBS) -l$(LOAD)

# Make 'all' the default goal, to prevent 'parallel' from becoming the
# default goal because it appears as first default goal candidate in
# this file.
.DEFAULT_GOAL := all

COMMON_TEX_FILE := $(LATEX_PROJECT_ROOT)/common.tex
BIB_FILE := $(LATEX_PROJECT_ROOT)/bibliography.bib

EXTRA_TEX_PATHS += $(LATEX_PROJECT_ROOT)
EXTRA_TEX_PATHS += $(TEX_FIGURES_PATH)
EXTRA_TEX_PATHS += $(GRAPHICS_PATH)

XDG_CONFIG_HOME ?= $(HOME)/.config
-include $(XDG_CONFIG_HOME)/latem/config.mk

-include $(LATEX_PROJECT_ROOT)/template-common.mk

RUBBER_TEXPATH_ARGS := $(foreach extraTexPath, $(EXTRA_TEX_PATHS), --texpath $(extraTexPath))
RUBBER_ARGS := --pdf --unsafe --warn all -c "biblatex.path $(LATEX_PROJECT_ROOT)" $(RUBBER_TEXPATH_ARGS)

# Those helper variables are required to handle whitespace in
# variables, we use $(space) below in PDFLATEX_TEXPATH_ARGS.
# See GNU Make Manual § 8.1 Function Call Syntax.
empty :=
space := $(empty) $(empty)

PDFLATEX_ARGS := -shell-escape
LATEXMK_ARGS := -shell-escape

# Invoking latexmk with -interaction=nonstopmode will automatically
# pass this option to the underlying pdflatex/lualatex invocation. As
# result, pdflatex/lualatex will not drop into an interactive shell on
# errors, but simply return with an exit status indication an error.
LATEXMK_ARGS += -interaction=nonstopmode

LATEXMK_ARGS += -halt-on-error

# Note that this will lead to an trailing ':' at the end of the
# texpath, which we keep deliberately as it means that the system
# texpaths are also included. Note how we use 'subst' to remove the
# spaces procuded by foreach (see description of the foreach function
# in the GNU Make Manual).
LATEX_TEXPATH_ARGS := $(subst $(space),,$(foreach extraTexPath,$(EXTRA_TEX_PATHS),$(extraTexPath):))
TEXINPUTS_ENV := TEXINPUTS=".:$(LATEX_TEXPATH_ARGS)"
BIBINPUTS_ENV := BIBINPUTS="$(LATEX_PROJECT_ROOT)"

ifneq (,$(findstring B, $(MAKEFLAGS)))
    RUBBER_ARGS += --force
	LATEXMK_ARGS += -g
endif

ifndef LATEX_BUILD_TOOL
ifneq (, $(shell which latexmk 2> /dev/null))
	LATEX_BUILD_TOOL := latexmk
else ifneq (, $(shell which rubber 2> /dev/null))
	LATEX_BUILD_TOOL := rubber
else
	LATEX_BUILD_TOOL := 3pdflatex
endif
endif

ifndef LATEX_ENGINE
	LATEX_ENGINE = pdflatex
endif

ifeq ($(LATEX_ENGINE),lualatex)
	TEX_TO_PDF_TOOL = lualatex
	LATEXMK_ARGS += -lualatex
	RUBBER_ARGS += -m lualatex
else ifeq ($(LATEX_ENGINE),pdflatex)
	TEX_TO_PDF_TOOL = pdflatex
	LATEXMK_ARGS += -pdf
else
$(error Unknown LATEX_ENGINE: $(LATEX_ENGINE))
endif

ifneq (,$(wildcard $(COMMON_TEX_FILE)))
EXTRA_PDF_PREREQUISITES += $(COMMON_TEX_FILE)
endif
ifneq (,$(wildcard $(BIB_FILE)))
EXTRA_PDF_PREREQUISITES += $(BIB_FILE)
endif

ifeq ($(BUILD_WERROR),true)
	LATEXMK_ARGS += -Werror
endif

RUBBER_BIN ?= rubber

# Generic rule how to build PDFs from TEX files using rubber, latexmk
# or alternaitvely 3x pdflatex.
%.pdf: SHELL=/bin/bash
%.pdf: TEX_FILE_DIR=$(dir $@)
ifeq ($(LATEM_BUILD_MODE),outoftree)
%.pdf: OUTDIR=$(TEX_FILE_DIR)/build
else
%.pdf: OUTDIR=$(TEX_FILE_DIR)
endif
%.pdf: %.tex $(EXTRA_PDF_PREREQUISITES)
	if [[ ! -d $(OUTDIR) ]]; then mkdir -p $(OUTDIR); fi
ifeq ($(LATEX_BUILD_TOOL),3latex)
	$(TEXINPUTS_ENV) $(TEX_TO_PDF_TOOL) $(PDFLATEX_ARGS) -output-directory ${OUTDIR} $<
	(test -f $(basename $@).bib && bibtex $(basename $@)) || true
	$(TEXINPUTS_ENV) $(TEX_TO_PDF_TOOL) $(PDFLATEX_ARGS) -output-directory $(OUTDIR) $<
	$(TEXINPUTS_ENV) $(TEX_TO_PDF_TOOL) $(PDFLATEX_ARGS) -output-directory $(OUTDIR) $<
else ifeq ($(LATEX_BUILD_TOOL),rubber)
	$(RUBBER_BIN) $(RUBBER_ARGS) $(RUBBER_EXTRA_ARGS) --into $(OUTDIR) $< 2> >(tee "$(basename $@).rubber-errors.log" >&2)
else ifeq ($(LATEX_BUILD_TOOL),latexmk)
	$(TEXINPUTS_ENV) $(BIBINPUTS_ENV) latexmk -output-directory=$(OUTDIR) $(LATEXMK_ARGS) $(LATEXMK_EXTRA_ARGS) $<
else
	$(error Unknown LATEX_BUILD_TOOL: $(LATEX_BUILD_TOOL))
endif

ifeq ($(LATEM_BUILD_MODE),outoftree)
	# If this is a out-of-tree build, then we need to move the target
	# back right next to the tex file, to avoid the make recipe being
	# out-of-date all the time.
	mv $(OUTDIR)/$@ $(TEX_FILE_DIR)
endif


# Combination of
# - https://unix.stackexchange.com/a/93971/6993
# - https://superuser.com/a/164857/18192
# with compatibitliy level set to '1.7'
%-grayscale.pdf: %.pdf
	gs -sOutputFile=$@ -q -dNOPAUSE -dBATCH -dSAFER -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dEmbedAllFonts=true -dSubsetFonts=true -sColorConversionStrategy=Gray -sColorConversionStrategyForImages=Gray -sProcessColorModel=DeviceGray $<

ENABLE_ANNOTATIONS_STRING := \def\MyEnableAnnotations{true}
ENABLE_DRAFT_STRING := \PassOptionsToClass{draft}{scrbook} \PassOptionsToPackage{draft}{graphicx}

%-with-annotations.tex: %.tex
	$(LATEX_PROJECT_ROOT)/ecat.sh -t '$(ENABLE_ANNOTATIONS_STRING)' -f $< > $@

%-as-draft.tex: %.tex
	$(LATEX_PROJECT_ROOT)/ecat.sh -t '$(ENABLE_DRAFT_STRING)' -f $< > $@

%-with-annotations-as-draft.tex: %.tex
	$(LATEX_PROJECT_ROOT)/ecat.sh \
		-t '$(ENABLE_ANNOTATIONS_STRING)' \
		-t '$(ENABLE_DRAFT_STRING)' \
		-f $< > $@

%-anonymous.tex: %.tex
	$(LATEX_PROJECT_ROOT)/ecat.sh -t '\def\MyMakeAnonymous{true}' -f $< > $@

.PHONY: parallel

parallel:
	$(MAKE) $(MAKE_PARALLEL_ARGS) all
