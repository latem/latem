#!/usr/bin/env bash

set -eo pipefail

declare -a TEXT
declare -a FILES

echoerr() { printf "%s\n" "$*" >&2; }

printHelp() {
	cat <<EOF
usage: $(basename "$0") [-d] [-f <file>] [-h] [-t <text>]
	-d: debug output
	-h: help text
	-f <file>: File to stdout
	-t <text>: Text to stdout
EOF
}

while getopts dhf:t: OPTION "$@"; do
	case $OPTION in
	d)
		set -x
		;;
	f)
		if [[ ! -f ${OPTARG} ]]; then
			echoerr "${OPTARG} ist not a file"
			printHelp
			exit 2
		fi
		FILES+=("${OPTARG}")
		;;
	h)
		printHelp
		exit
		;;
	t)
		TEXT+=("${OPTARG}")
		;;
	*)
		echoerr "Invalid option ${OPTION}"
		printHelp
		exit 1
		;;
	esac
done

if [[ ${#TEXT[@]} -gt 0 ]]; then
	TMPDIR=$(mktemp -d)
	trap 'rm -rf ${TMPDIR}' EXIT

	for textIndex in ${!TEXT[@]}; do
		TEXT_FILE="${TMPDIR}/${textIndex}"
		echo "${TEXT[$textIndex]}" > "${TEXT_FILE}"
		FILES=("${TEXT_FILE}" "${FILES[@]}")
	done
fi

cat "${FILES[@]}"
