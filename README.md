# LaTeM - The *La*TeX *Te*mplate *M*anager

Create LaTeX documents by instanciating templates.

```bash
$ ./latem paper-acmart ~/papers/my-novel-idea
```

## Out-of-Tree Builds

LaTeM projects default to in-tree builds, but can be configured to
perform out-of-tree builds. Simply set LATEM_BUILD_MODE=outoftree. This
can be set globally in `${XDG_CONFIG_HOME}/latem/config.mk`:

```bash
echo LATEM_BUILD_MODE=outoftree >> "${XDG_CONFIG_HOME:-~/.config}"/latem/config.mk
```

## License

This project is licensed under the MIT License. See the file `LICENSE`
in the project's root directory.
